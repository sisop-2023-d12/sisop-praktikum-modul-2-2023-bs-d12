/* Soal 2 */

#define _XOPEN_SOURCE 500 // to enable nftw

#include <errno.h>     // errno, EACCES, ENOENT, ENOEXEC
#include <ftw.h>       // nftw
#include <signal.h>    // signal, SIGQUIT, SIGTERM, SIGINT, SIGUSR1
#include <stdbool.h>   // bool
#include <stdio.h>     // printf, fprintf, stderr, snprintf
#include <stdlib.h>    // EXIT_FAILURE, EXIT_SUCCESS, NULL
#include <string.h>    // strncmp
#include <sys/stat.h>  // umask
#include <sys/types.h> // pid_t
#include <sys/wait.h>  // wait, WIFEXITED, WEXITSTATUS
#include <time.h>      // time, time_t
#include <unistd.h>    // STDIN_FILENO, STDOUT_FILENO, STDERR_FILENO

#define IS_PID_CHILD(pid) (pid == 0)

/** Executable file of daemon killer program */
char *killer_exec = "lukisan-kill-daemon";

/** Path where the executable is called
 * (not where the executable is). This array is
 * dynamically allocated, so free it at the end.
 */
char *summoned_path;

/** Determines whether background processes
 *  terminate immediately when stopped (MODE_A)
 *  or leave them running until they are finished (MODE_B)
 *
 *  MODE_A = false
 *  MODE_B = true
 */
bool linger = true;

/** Wrapper for remove() */
int f_remove(const char *fpath, const struct stat *sb, int typeflag,
             struct FTW *ftwbuf) {
  int r = remove(fpath);
  if (r && typeflag & FTW_D)
    perror("Unable to remove directory");
  if (r && typeflag & FTW_F)
    perror("Unable to remove file");
  return r;
}

/** The logic loop run for each folder generated
 *
 * 1. Create folder with the current date and time
 *    with the format `YYYY-mm-dd_HH:mm:ss`
 * 2. Every 5 seconds download an image from picsum.photos
 *    with size `(t%1000)+50` px (`t` is UNIX Epoch)
 * 3. After 15 images are downloaded,
 *    - zip the images and place it outside the folder
 *    - delete the folder along with its contents
 *
 * Therefore, each process runs 15*5=75 seconds.
 * By the time it finishes, two more processes spawned.
 * If this keeps going, resources will become scarce.
 * This is why we have that "killer" program.
 */
void folder_download(pid_t pid) {
  /** Number of images to download per folder */
  const int IMG_COUNT = 15;
  /** Time in seconds to spawn a new process to
   * download a new image */
  const int FILE_DELAY_SEC = 5;
  /** Length of timestamp format
   * which is "YYYY-mm-dd_HH:mm:ss"
   * plus one null terminator
   */
  const int FORMAT_LEN = 20;

  /*** 1. CREATE FOLDER ***/

  int cwdlen = strlen(summoned_path);
  // Path length, accomodating for folder length and null.
  // +1 is for the slash (_/_)
  int folderpathlen = cwdlen + FORMAT_LEN + 1;
  // Folder path, dynamically allocated.
  // Please `free` on exiting folder process.
  char *folder_path = calloc(sizeof(char), folderpathlen);
  // exit if failed to allocate
  if (folder_path == NULL)
    exit(EXIT_FAILURE);

  time_t t_folder = time(NULL);
  struct tm *tm_folder = localtime(&t_folder);
  // TODO use strptime or strftime
  snprintf(folder_path, folderpathlen, "%s/%04d-%02d-%02d_%02d:%02d:%02d",
           summoned_path, tm_folder->tm_year + 1900, tm_folder->tm_mon,
           tm_folder->tm_mday, tm_folder->tm_hour, tm_folder->tm_min,
           tm_folder->tm_sec);

  // exit if unable to make folder
  if (mkdir(folder_path, S_IRWXU) != 0) {
    free(folder_path);
    exit(EXIT_FAILURE);
  }

  /*** 2. DOWNLOAD IMAGES ***/
  for (int i = 0; i < IMG_COUNT;) {
    pid_t pid_file = fork();

    if (pid_file < 0) {
      free(folder_path);
      exit(EXIT_FAILURE);
    } else if (IS_PID_CHILD(pid_file)) {
      // child download one image from the internet

      /*** 2a. SETUP PATH ***/

      // Path length, accomodating for file and folder length and null.
      // +5 is for the extra slash and the extension (_/_/_.jpg)
      int filepathlen = folderpathlen + FORMAT_LEN + 5;

      // Folder path, dynamically allocated.
      // Please `free` on exiting folder process.
      char *file_path = calloc(sizeof(char), filepathlen);
      if (file_path == NULL)
        exit(EXIT_FAILURE);

      time_t t_file = time(NULL);
      struct tm *tm_file = localtime(&t_file);

      // TODO use strptime or strftime
      snprintf(file_path, filepathlen, "%s/%04d-%02d-%02d_%02d:%02d:%02d.jpg",
               folder_path, tm_file->tm_year + 1900, tm_file->tm_mon,
               tm_file->tm_mday, tm_file->tm_hour, tm_file->tm_min,
               tm_file->tm_sec);

      /*** 2b. CALL CURL ***/

      // Download URL
      char url[27];

      // Image download size in pixels
      int px = (t_file % 1000) + 50;

      snprintf(url, 27, "https://picsum.photos/%d", px);
      char *curl_argv[] = {"curl", "-sLo", file_path, url, NULL};

      execvp("curl", curl_argv);

      // If you got here, curl failed to execute.
      switch (errno) {
      case EACCES:
      case ENOENT:
      case ENOEXEC:
        // exit with fatal error
        free(file_path);
        exit(2);
        break;
      default:
        free(file_path);
        exit(EXIT_FAILURE);
        break;
      }
    }
    // Parent wait for child to finish (downloading)

    // Status of the child
    int status_file;
    wait(&status_file);
    // Keep track of successful downloads (return 0)
    if (WIFEXITED(status_file)) {
      switch (WEXITSTATUS(status_file)) {
      // on success, increase count, go again
      case 0: // EXIT_SUCCESS
        ++i;
        break;
      // on fatal error, stop trying
      case 2:
        free(folder_path);
        exit(EXIT_FAILURE);
        break;
      // otherwise retry
      default: // EXIT_FAILURE or others
        break;
      }
    }
    sleep(FILE_DELAY_SEC);
  }

  /*** 3. ZIP IMAGES ***/

  pid_t pid_zip = fork();

  if (pid_zip < 0) {
    free(folder_path);
    exit(EXIT_FAILURE);
  } else if (IS_PID_CHILD(pid_zip)) {
    // child execute zip

    // Length of zip file, based on forder path
    // but ends with .zip extension, adding 4 chars
    int zipfilelen = folderpathlen + 4;

    char *zipfile = calloc(sizeof(char), zipfilelen);
    if (zipfile == NULL) {
      exit(EXIT_FAILURE);
    }

    snprintf(zipfile, zipfilelen, "%s.zip", folder_path);

    char *zip_argv[] = {"zip", "-r", zipfile, folder_path, NULL};
    execvp("zip", zip_argv);

    // something's wrong when trying to execute zip
    exit(EXIT_FAILURE);
  }
  // parent wait for zip to finish

  // zip status
  int status_zip;
  wait(&status_zip);
  // aditional processing if necessary
  if (WIFEXITED(status_zip) && WEXITSTATUS(status_zip) == 0) {
    // delete folder, but cannot use rmdir, so
    if (nftw(folder_path, f_remove, 64, FTW_DEPTH | FTW_PHYS) != 0) {
      // cannot remove certain file/directory
      exit(EXIT_FAILURE);
    }
  }

  /*** 99. CLEANUP ***/
  free(folder_path);
}

/** Create a folder every 30 seconds, each folder calls folder_download */
void spawn_folder() {
  pid_t pid_folder = fork();

  if (pid_folder < 0) {
    exit(EXIT_FAILURE);
  } else if (IS_PID_CHILD(pid_folder)) {
    folder_download(pid_folder);
    exit(EXIT_SUCCESS);
  }
}

/** Process to create a program that kills the daemon
 *
 * 1. Create killer_src file
 * 2. Compile killer_exec executable
 * 3. Delete killer_src file
 *
 */
int generateKiller(pid_t pid_to_kill) {
  // Embedded C code to kill a program
  const char *code =
      "#include <signal.h>\n#include <unistd.h>\n#include <stdlib.h>\n#include "
      "<limits.h>\nmain(int c,char*v[]){kill(%d,10);char "
      "f[4096];realpath(v[0],f);unlink(f);}\n";
  char *killer_src = "killer.c";
  char *cxx = "gcc";
  // Compiler flags
  // -Wno-implicit-int prevents leaking source
  char *cxx_args[] = {cxx,         killer_src,          "-o",
                      killer_exec, "-Wno-implicit-int", NULL};

  /*** 0. Prepare fork for GCC process ***/

  pid_t pid_gcc = fork();

  if (pid_gcc < 0) {
    fprintf(stderr, "Unable to create process for compilation.\n");
    return 1;
  }

  // execute in GCC process
  if (IS_PID_CHILD(pid_gcc)) {
    /*** 1. Create killer_src file ***/

    // Source file, open for write and then read
    FILE *srcfile = fopen(killer_src, "w+");
    if (srcfile == NULL) {
      // fprintf(stderr, "Unable to write temporary file.\n");
      perror("Unable to write temporary file");
      exit(EXIT_FAILURE);
    }

    // Write code to file, replacing %d in the code with pid_to_kill
    fprintf(srcfile, code, pid_to_kill);
    fclose(srcfile);

    /*** 2. Compile killer_exec file ***/

    execvp(cxx, cxx_args);
  }

  // Return value of the GCC process
  int status;

  // Wait until compilation finishes
  wait(&status);

  /*** 3. Delete killer_src file ***/
  remove(killer_src);

  // check for completion status
  if (WIFEXITED(status)) {
    return WEXITSTATUS(status);
  } else {
    return -1;
  }
}

/** Function to handle incoming signals */
void signal_handler(int signum) { kill(0, SIGTERM); }

/** Cleaning up the process upon exit */
void cleanup() { free(summoned_path); }

/* The main process

1. Fork a daemon process, exit if fail
2. Initialize the daemon, exit if fail
3. Generate killer program, exit if fail
4. Run daemon logic loop
*/
int main(int argc, char *argv[]) {
  /*** 0. PARSE MODE ***/
  if (argc < 2) {
    printf(
        "Usage: %s [MODE]\n"
        "\n"
        "MODE:\n"
        "\t-a\tWhen daemon is killed, kill all running child processes.\n"
        "\t-b\tWhen daemon is killed, keep all child processes running.\n"
        "\n"
        "When this program is run, it will generate a daemon killer program.\n"
        "It will send SIGUSR1 to the daemon and kill it. How its child "
        "processes\n"
        "will be handled depends on the mode set initially here.\n"
        "The daemon killer program is: %s\n"
        "\n",
        argv[0], killer_exec);

    return 1;
  }

  if (strncmp("-a", argv[1], 3) == 0)
    linger = false;
  else if (strncmp("-b", argv[1], 3) == 0)
    linger = true;
  else {
    printf("ERROR: invalid mode %s\n", argv[1]);
    return 1;
  }

  /*** 1. FORK A DAEMON ***/
  // exit if fail

  printf("Launching daemon process in %s...\n", linger ? "MODE_B" : "MODE_A");

  summoned_path = getcwd(NULL, 0);
  atexit(cleanup);

  // Daemon process PID
  pid_t pid_daemon = fork();

  // Exit when fork fails
  if (pid_daemon < 0) {
    fprintf(stderr, "Unable to launch daemon. Exiting.\n");
    exit(EXIT_FAILURE);
  }

  // Exit parent process if it is successful
  if (pid_daemon > 0) {
    exit(EXIT_SUCCESS);
  }

  /*** WE ARE NOW IN THE DAEMON PROCESSES ***/

  /*** 2. INITALIZE DAEMON ***/

  pid_daemon = getpid();

  // Gain full access to all files made by the child process
  umask(0);

  // Child session SID
  pid_t sid_daemon = setsid();
  if (sid_daemon < 0) {
    fprintf(stderr, "Unable to create session for daemon. Exiting.\n");
    exit(EXIT_FAILURE);
  }

  /*** 3. GENERATE KILLER ***/
  if (generateKiller(pid_daemon) != 0) {
    fprintf(stderr, "Unable to compile killer program for daemon. Exiting.\n");
    exit(EXIT_FAILURE);
  }

  /*** 4. RUN DAEMON ***/

  // Try to move to root directory, a directory that ALWAYS exists
  // in all POSIX-compliant operating systems.
  if (chdir("/") < 0) {
    fprintf(stderr,
            "Unable to persist session resource for daemon. Exiting.\n");
    exit(EXIT_FAILURE);
  }

  if (!linger) {
    signal(SIGUSR1, signal_handler);
  }

  printf("Daemon launched with PID %d.\n", pid_daemon);

  // Close stdin, stdout, and stderr, daemons don't need them
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  /** Time in seconds to spawn a new process that
   * downloads images to a folder
   */
  const int FOLDER_DELAY_SEC = 30;
  // Run the daemon process
  while (true) {
    spawn_folder();
    sleep(FOLDER_DELAY_SEC);
  }

  return 0;
}