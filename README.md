# Sisop Praktikum Modul 2 2023 - BS - D12

Shell Scripting, Cron, dan AWK

Sistem Operasi Semester Genap 2023
Institut Teknologi Sepuluh Nopember
Kelompok D12

| ID  | Nama                        | NRP            |
| --- | --------------------------- | -------------- |
| MH  | Muhammad Hidayat            | 05111940000131 |
| BS  | Muhammad Budhi Salmanjannah | 5025201084     |

Semua soal melarang (_ban_) penggunaan fungsi `system()` dari header `stdlib.h`, namun tidak melarang penggunaan fungsi `exec()` dan sejenisnya.

## Soal 1

## Soal 2

The program depends on `gcc`, `curl`, and `zip` to compile killer program, download images, and compress folders respectively.

Process tree:
```
- main
  - daemon
    - gcc
    - folder*
      - file_download*
```

When main is killed, `daemon` runs in the background. `gcc` ends before `folder*` starts spawning. When daemon is killed, `folder*` remains running. This is the behavior of `MODE_B` and nothing else needs to be done implementation-wise. To implement `MODE_A`, which is to kill `folder*` and all `file_download*` when `daemon` is killed, the daemon must propagate its SIGTERM to its process group, ending all their processes and stoppping any ongoing processes.

I have tried to send SIGTERM from the killer program, but since I am also handling SIGTERM to itself and all child processes, it ended up in a loop. So I changed it so that the killer sends SIGUSR1 instead which will then be handled by the daemon.

Currently the resulting zip file puts out the absolute path from `/` into the archive. Example is the image below:

![Zipped archive of downloaded images](./img/02_01.png)

When `folder_download` child finishes, the process becomes a zombie. Waiting for this child to finish to prevent zombie processes means downloads per folder will not be parallel as recommended.

Potential improvements:
- use `mkstemp` to write the generated code, and use `write` to write the file

## Soal 3

## Soal 4

Not much progress other than the program can parse the arguments and report errors as intended. As for the cron-like implementation, still got some work to make it work, and I intend to do it using long delays than checking every second.

Depending on whether the hour, minute, or second is either `*` or a number, there are a total of 8 cases:

- `* * *`: (easy) run every second

  next valid: next second

- `* m *`: (hard) run every second at minute `m` regardless of hour

  next valid: next second or next hour at second 0

- `h * *`: (normal) run every second at hour `h`

  next valid: next second or next day at minute 0 second 0

- `h m *`: (normal) run every second at `h:m`

  next valid: next second or next day at second 0

- `* * s`: (easy) run every minute at second `s`

  next valid: next minute

- `* m s`: (easy) run every hour at minute `m`, second `s`

  next valid: next hour

- `h * s`: (hard) run every minute at hour `h`, second `s`

  next valid: next minute or next day at minute 0

- `h m s`: (easy) run at exact hour, minute, and second

Assume the current time is 7:21:35 PM.

### Case 1 (easy): `* * *`

Simply put, run every second.

The schedule would be:

- 19:21:36
- 19:21:37
- ...
- 19:21:59
- 19:22:00 `// the minute changes`
- 19:22:01
- ...
- 19:59:59
- 20:00:00 `// the hour changes`
- 20:00:01
- ...
- 23:59:59
- 00:00:00 next day
- 00:00:01
- ...

### Case 2 (easy): `* * 0`

Simply put, run every minute at 0th second.

The schedule would be:

- 19:22:00 `// start at 22nd minute`
- 19:23:00
- ...
- 19:59:00
- 20:00:00 `// the hour changes`
- 20:01:00
- ...
- 23:59:00
- 00:00:00 next day
- 00:01:00
- ...

### Case 3 (easy): `* 15 0`

Simply put, run every hour at `__:15:00`.

The schedule would be:

- 20:15:00 `// start at the following hour`
- 21:15:00
- ...
- 23:15:00
- 00:15:00 next day
- 01:15:00

### Case 4 (easy): `18 13 36`

Run every day at `18:13:36`. That's it. Past that time, run the next day.

### Case 5 (normal): `21 * *`

Run every second at `21:__:__`. Past that time, run the next day.

### Case 6 (normal): `21 12 *`

Run every second at `21:12:__`. Past that time, run the next day.

### Case 7 (hard): `* 23 *`

The schedule would be:

- 19:23:00 `// start at the 23rd minute at the first possible second`
- 19:23:01
- 19:23:02
- ...
- 19:23:59
- 20:23:00 `// next hour at the 23rd minute at the first possible second`
- 20:23:01
- 20:23:02
- ...
- 23:23:59
- 00:23:00 next day `// next hour at the 23rd minute at the first possible second`
- 00:23:01
- ...

### Case 8 (hard): `18 * 12`

- 18:00:12
- 18:01:12
- 18:02:12
- ...
- 18:59:12
- 18:00:12 next day
- 18:01:12

Reference: 
- [How do you call a function once a day at a certain time in c++?](https://stackoverflow.com/q/4442597)
- [Executing a function at specific intervals](https://stackoverflow.com/q/15463621)