CXX=gcc
CXXFLAGS=-g

.PHONY: clean run all

run:
	$(target) $(ARGS)

clean:
	rm *.o

all: $(target)

soal1: soal1/binatang.o

soal2: soal2/lukisan.o

soal3: soal3/filter.o

soal4: soal4/mainan.o

%.o: %.c Makefile
	$(CXX) $(CXXFLAGS) -o $@ $< 