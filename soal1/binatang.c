#include <errno.h>    
#include <ftw.h>      
#include <signal.h>    
#include <stdbool.h>   
#include <stdio.h>     
#include <stdlib.h>    
#include <string.h>    
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>


int main(){
	curl --silent --libcurl /soal1/grape.c 'https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq';
	cd /soal1/;
	find . -name 'binatang.zip' -execdir unzip {}\;
	cd /binatang/;

	return 0;
}

