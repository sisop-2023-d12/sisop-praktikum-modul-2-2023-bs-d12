# Soal Shift Modul 2 Sistem Operasi 2023

[Soal asli](https://docs.google.com/document/d/1XP8xG5uaBfsujyngqTTgNqAWAEteUlh62J2H2X-2r-k/preview)

1. Waktu pengerjaan dimulai Senin (20/3) pukul 10.00 WIB hingga Sabtu (25/3) pukul 22.00 WIB.
2. Praktikan diharapkan membuat laporan penjelasan dan penyelesaian soal dalam bentuk Readme(gitlab).
3. Format nama repository gitlab `sisop-praktikum-modul-${Nomor_Modul}-2023-${Kode_Dosen_Kelas}-${Nama_Kelompok}` (contoh: `sisop-praktikum-modul-2-2023-WS-A11`).
4. Struktur repository seperti berikut:
   - soal1:
     - binatang.c
   - soal2:
     - lukisan.c
   - soal3:
     - filter.c
   - soal4:
     - mainan.c

   Jika melanggar struktur repo akan dianggap sama dengan curang dan menerima konsekuensi sama dengan melakukan kecurangan.
5. Setelah pengerjaan selesai, semua script bash, awk, dan file yang berisi cron job ditaruh di gitlab masing - masing kelompok, dan link gitlab diletakkan pada form yang disediakan.
6. Commit terakhir maksimal 10 menit setelah waktu pengerjaan berakhir. Jika melewati maka akan dinilai berdasarkan commit terakhir.
7. Jika tidak ada pengumuman perubahan soal oleh asisten, maka soal dianggap dapat diselesaikan.
8. Jika ditemukan soal yang tidak dapat diselesaikan, harap menuliskannya pada Readme beserta permasalahan yang ditemukan.
9. Praktikan tidak diperbolehkan menanyakan jawaban dari soal yang diberikan kepada asisten maupun praktikan dari kelompok lainnya.
10. Jika ditemukan indikasi kecurangan dalam bentuk apapun di pengerjaan soal shift, maka nilai dianggap 0.
11. Pengerjaan soal shift sesuai dengan modul yang telah diajarkan.
12. Zip dari repository dikirim ke email asisten penguji dengan subjek yang sama dengan nama judul repository, dikirim sebelum deadline dari soal shift

## Soal 1: Grape-kun at the Zoo

Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang. Sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga. Dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari [drive kebun binatang tersebut](https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq).

- Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.
- Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.
- Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama `HewanDarat`, `HewanAmphibi`, dan `HewanAir`. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
- Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

Catatan:
Untuk melakukan zip dan unzip tidak boleh menggunakan `system()`

## Soal 2: Sucipto mencari ide

Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karyanya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi!

- Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp `YYYY-MM-dd_HH:mm:ss`.
- Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp `YYYY-mm-dd_HH:mm:ss`.
- Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete (sehingga hanya menyisakan .zip).
- Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
- Buatlah program utama bisa dirun dalam dua mode, yaitu `MODE_A` dan `MODE_B`. untuk mengaktifkan `MODE_A`, program harus dijalankan dengan argumen `-a`. Untuk `MODE_B`, program harus dijalankan dengan argumen `-b`. Ketika dijalankan dalam `MODE_A`, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk `MODE_B`, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai (semua folder terisi gambar, terzip lalu di delete).

Catatan :
- Tidak boleh menggunakan `system()`
- Proses berjalan secara daemon
- Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

## Soal 3: Ten Hag to be Man Utd Manager?

Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”

- Pertama-tama, Program `filter.c` akan mengunduh file yang berisikan [database para pemain bola](https://drive.google.com/file/d/1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF/view?usp=share_link). Kemudian dalam program yang sama diminta dapat melakukan extract `players.zip`. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
- Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.
- Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan **posisi** mereka dalam **waktu bersamaan dengan 4 proses yang berbeda**. Untuk kategori folder akan menjadi 4 yaitu **Kiper**, **Bek**, **Gelandang**, dan **Penyerang**.
- Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan **Kesebelasan Terbaik** untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi `Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt` dan akan ditaruh di `/home/$USER/`

Catatan:
- Format nama file yang akan diunduh dalam zip berupa [nama]_[tim]_[posisi]_[rating].jpg
- Tidak boleh menggunakan system()
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec() dan fork().
- Directory “.” dan “..” tidak termasuk yang akan dihapus.
- Untuk poin d **DIWAJIBKAN** membuat fungsi `bernama buatTim(int, int, int)`, dengan input 3 value integer dengan urutan bek, gelandang, dan striker.


## Soal 4: Banabil Melatih Fokus

Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.

Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

- Banabil tidak ingin menggunakan fungsi `system()`, karena terlalu mudah.
- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
Bonus poin apabila CPU state minimum.

Contoh untuk run: `./program \* 44 5 /home/Banabil/programcron.sh`
