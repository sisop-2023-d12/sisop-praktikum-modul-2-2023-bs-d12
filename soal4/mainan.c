/* Soal 4 */

#define _XOPEN_SOURCE 500 // to enable nftw

#include <limits.h> // PATH_MAX
#include <stdio.h>  // printf, sscanf
#include <stdlib.h> // calloc, realpath
#include <string.h> // sscanf
// #include <unistd.h>

char *summoned_path;

#define EVERY_HOUR 4
#define EVERY_MIN  2
#define EVERY_SEC  1

int get_current_time(int hour, int min, int sec) {}

// TODO untested, may not compile
void run_program(int hour, int min, int sec, char *program, int argc,
                 char *argv[]) {
  time_t t_now, t_target;
  struct tm *tm_now;
  int *t_hour = &(tm_now->hour);
  int *t_min = &(tm_now->min);
  int *t_sec = &(tm_now->sec);

  while (1) {
    int delay;
    t_now = time(NULL);
    tm_now = gmtime(&t_now);

    int flag = ((hour >= 0) << 2 | (min >= 0) << 1 | (sec >= 0));

    if (flag & EVERY_SEC) {
      /*
        spec  :  *  *  *
        target: 18 37 43
        now   : 18 37 42
      */
      delay = 1;
      if (flag & EVERY_MIN && *t_hour != hour) {
        /*
          spec  : 21  *  *
          target: 21 00 00 (+)
          now   : 18 37 42
        */
        delay = time_to_next_hour + time_to_nearest_minute + time_to_nearest_second
      }
      if (flag & EVERY_HOUR && *t_min != min) {
        /*
          spec  :  * 12  *
          target: 19 12 00 (+35m)
          now   : 18 37 42
        */
      }
    }

    if (sec >= 0)
      *t_sec = sec;
    else {
      *t_sec = (*t_sec + 1) % 59;
    }

    if (min >= 0)
    *t_min = min;
    else {
      *t_min = (*t_min + 1) % 59;
      tm_target->tm_sec = (sec >= 0) ? sec : 0;
    }

      tm_target->tm_hour = (hour < 0) ? (tm_target->tm_hour + 1) % 23 : hour;
    tm_target->tm_hour = hour;
    tm_target->tm_sec = sec;
  }
}

int main(int argc, char *argv[]) {
  /*** 0. PARSE MODE ***/
  if (argc < 5) {
    printf("Usage: %s <hour> <min> <sec> <program> [args...]\n"
           "\n"
           "`hour`   : 0-23 or * for every hour\n"
           "`min`    : 0-59 or * for every minute\n"
           "`sec`    : 0-59 or * for every second\n"
           "`program`: Program to execute\n"
           "`args`   : Arguments to the program\n"
           "\n",
           argv[0]);

    return 1;
  }

  int hour, _hour, min, _min, sec, _sec;
  char program[PATH_MAX];
  int program_argc = argc - 5;
  char **program_argv = calloc(sizeof(char *), program_argc + 1);

  if (program_argv == NULL) {
    perror("Unable to parse arguments");
    return 1;
  }

  // parse time
  if (strcmp("*", argv[1]) == 0)
    hour = -1;
  else {
    sscanf(argv[1], "%d", &_hour);
    if (_hour < 0 || _hour > 23) {
      printf("Hour %d is invalid, it must be in the range of 0-23.\n", _hour);
      return 1;
    } else
      hour = _hour;
  }
  if (strcmp("*", argv[2]) == 0)
    min = -1;
  else {
    sscanf(argv[2], "%d", &_min);
    if (_min < 0 || _min > 59) {
      printf("Hour %d is invalid, it must be in the range of 0-59.\n", _min);
      return 1;
    } else
      min = _min;
  }
  if (strcmp("*", argv[3]) == 0)
    sec = -1;
  else {
    sscanf(argv[3], "%d", &_sec);
    if (_sec < 0 || _sec > 59) {
      printf("Hour %d is invalid, it must be in the range of 0-59.\n", _sec);
      return 1;
    } else
      sec = _sec;
  }
  // get full path so that we don't need to worry about workdir
  realpath(argv[4], program);

  for (int i = 5; i < argc; ++i) {
    program_argv[i - 5] = argv[i];
  }

  printf("On %02d:%02d:%02d, execute:\n%s ", hour, min, sec, program);

  for (int i = 0; i < program_argc; ++i) {
    printf("%s ", program_argv[i]);
  }
  printf("\n");

  /*** 1. FORK A DAEMON ***/
  // exit if fail

  printf("Launching daemon process..\n");

  summoned_path = getcwd(NULL, 0);
  // atexit(cleanup);

  // Daemon process PID
  pid_t pid_daemon = fork();

  // Exit when fork fails
  if (pid_daemon < 0) {
    fprintf(stderr, "Unable to launch daemon. Exiting.\n");
    exit(EXIT_FAILURE);
  }

  // Exit parent process if it is successful
  if (pid_daemon > 0) {
    exit(EXIT_SUCCESS);
  }

  /*** WE ARE NOW IN THE DAEMON PROCESSES ***/

  /*** 2. INITALIZE DAEMON ***/

  pid_daemon = getpid();

  // Gain full access to all files made by the child process
  umask(0);

  // Child session SID
  pid_t sid_daemon = setsid();
  if (sid_daemon < 0) {
    fprintf(stderr, "Unable to create session for daemon. Exiting.\n");
    exit(EXIT_FAILURE);
  }

  /*** 3. RUN DAEMON ***/

  // Try to move to root directory, a directory that ALWAYS exists
  // in all POSIX-compliant operating systems.
  if (chdir("/") < 0) {
    fprintf(stderr,
            "Unable to persist session resource for daemon. Exiting.\n");
    exit(EXIT_FAILURE);
  }

  printf("Daemon launched with PID %d.\n", pid_daemon);

  // Close stdin, stdout, and stderr, daemons don't need them
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  /** Time in seconds to spawn a new process that
   * downloads images to a folder
   */
  // Run the daemon process
  run_program(hour, min, sec, program, program_argc, program_argv);

  return 0;
}